import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df

#Extract all Titles

def extract_titles(name):
    if 'Mr.' in name:
        return 'Mr.'
    elif 'Mrs.' in name:
        return 'Mrs.'
    elif 'Miss.' in name:
        return 'Miss.'
    else:
        return 'None'    


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    df['Title'] = df['Name'].apply(extract_titles) #Apply function extract_titles to all vlaues from dataset
    median_ages = df.groupby('Title')['Age'].median().round().astype(int).to_dict() #Calculating median age and add to dictionary
    missing_values = df['Age'].isnull().groupby(df['Title']).sum().astype(int).to_dict() #Calculating missing values and add to dictionary
    return [(title, missing_values[title], median_ages[title]) for title in ['Mr.', 'Mrs.', 'Miss.']] #Return title of the group, number of missed values for the group, median value calculated for the group
